# qmail-installer #

## Description ##
This repository contains a script called _qmail-installer_. This script is currently used to install _qmail_ on a personal linux-based distro. This distro is based on CentOs. The script at this moment install qmail (it downloads, compile and configure) and its dependencies (daemontools and ucspi-tcp) using a personal default-based configuration.  
The script will be update as soon as possibile to satisfy a user configuration of qmail.  

## How to install qmail using qmail-installer ##
To install qmail using qmail-installer script you only need to download it (using git or downloading the tar.gz archive) and run qmail-installer.sh

## Features ##
- Updated README  
- Italian README  
- Configurable setup  
- Documentation  
- Improvements to code (I wrote this script without knowing bash scripting so it is written in a bad way, I apologise but at this moment is designed only for a personal use)

## Funzionamento ##


