#!/bin/bash

# Directory in cui salvare i sorgenti di qmail e delle sue dipendenze (daemontools e ucspi)
qmail_source_code_path="/usr/src/qmail_source/"

qmail_installation_path="/usr/local/src/"
daemontools_package_installation_dir="/package/"

# Array contenente nome e versione dei software utilizzata per il download dei sorgenti (viene aggiunto un url statico iniziale e un ".tar.gz" finale)
netqmail="netqmail-1.06"
ucspi="ucspi-tcp-0.88"
daemontools="daemontools-0.76"

netqmail_source_url="http://www.qmail.org/"$netqmail".tar.gz"
ucspi_source_url="http://cr.yp.to/ucspi-tcp/"$ucspi".tar.gz"
daemontools_source_url="http://cr.yp.to/daemontools/"$daemontools".tar.gz"

ucspi_patch="ucspi-tcp-0.88-errno.patch"
daemontools_patch="daemontools-0.76-errno.patch"

qmail_startup_script="qmailctl-script-dt70"
qmail_startup_script_url="http://lifewithqmail.org/"$qmail_startup_script

sources_file_urls=($netqmail_source_url $ucspi_source_url $daemontools_source_url $qmail_startup_script_url)

# Lista di pacchetti che devono essere installati per la corretta installazione di qmail e delle sue dipendenze
dependencies=("cc" "tar" "gzip" "chmod" "cd" "mv" "rm" "ln" "make" "patch")

# Hostname usato per la configurazione "./config-fast" di qmail
hostname="chimera.local"

# Cosa fa		:		Stampa un messaggio a schermo
# $1			:		numerico, tipo di messaggio da stampare. 1 = INFO | 2 = WARNING | 3 = ERROR
# $2			:		stringa, messaggio da stampare
# $3			:		<opzionale> logico, true = Termina con exit code 1 | false = Non termina. Default = true
print_message(){
    info_message_header="INFO"
    warning_message_header="WARNING"
    error_message_header="ERROR" 
    
    exit_on_error=${3:-true}
    
    if [ $1 -eq 1 ]; then 	# Messaggio di info
	echo -e "\e[34m"$info_message_header"\e[0m|"$2
    elif [ $1 -eq 2 ] ; then	# Messaggio di warning
	echo -e "\e[33m"$warning_message_header"\e[0m|"$2 
    elif [ $1 -eq 3 ] ; then	# Messaggio di errore
	echo -e "\e[31m"$error_message_header"\e[0m|"$2
	if [ $exit_on_error = true ]; then
	    exit 1	# Esco e imposto come exit status 1 (Errore)
	fi 
    else
	echo $2
    fi
}

# Cosa fa		:		Crea la cartella che conterrà i sorgenti
function make_dir(){
    mkdir -p $1

    # Nel caso non sia stato possibile creare la cartella stampo un errore 
    if [ $? -ne 0 ]; then
	print_message	3 "Si è verificato un errore durante la creazione della cartella "$1". Tale cartella è necessaria per poter contenere i sorgenti di qmail e delle sue dipendenze." true
    else
	print_message 1 "Cartella "$1" creata correttamente" 
    fi 
}

# Cosa fa		:		Scarica i sorgenti dall'array di dati da scaricare.
# $1			:		Url dal quale scaricare il file
function download_sources(){
    print_message 1 "Inizio download da "$1" ..."
    wget $1
    if [ $? -ne 0 ]; then
	print_message 3 "Si è verificato un errore durante il download della risorsa dall'indirizzo: "$1
    else
	print_message 1 "Download terminato"
    fi
}

# Cosa fa		:		Controlla che l'utente utilizza sia l'utente root
function check_root_user(){
    if [ "$(id -u)" -ne "0" ]; then
	print_message	3 "Per eseguire il setup di qmail è necessario essere root" true
    fi 
}

# Cosa fa		:		Controlla che le dipendenze siano installate mediante l'utilizzo di "command"
function check_dependency(){

    if [ -n "$(command -v $1)" ]; then
	print_message 1 $1" OK"
    else 
	print_message 3 $1" NO" false
	print_message 3 "E' necessario disporre di tutte le dipendenze per poter installare qmail: "$dependencies true
    fi
}

# Cosa fa		:		Esplode i tar dei sorgenti nelle apposite cartelle
function explode_sources(){
    print_message 1 "Inizio procedura di estrazione dei sorgenti nelle apposite cartelle"

    tar -xzf $qmail_source_code_path$netqmail$".tar.gz" --directory $qmail_installation_path
    
    tar -xzf $qmail_source_code_path$ucspi".tar.gz" --directory $qmail_installation_path

    tar -xzf $qmail_source_code_path$daemontools$".tar.gz" --directory $daemontools_package_installation_dir

    if [ -d "$qmail_installation_path$netqmail" ] && [ -d "$qmail_installation_path$ucspi" ] && [ -d "$daemontools_package_installation_diradmin"]; then
	print_message 1 "Archivi estratti correttamente"
    else
	print_message 3 "Si è verificato un errore durante l'estrazione degli archivi" true
    fi 

}

# Cosa fa		:		Aggiunge gli utenti e i gruppi necessari per il funzionamento di qmail all'interno di sistemi basati su Linux
#					Vedi: /usr/local/src/netqmail-1.06/INSTALL.ids
function linux_add_users_group(){
    print_message 1 "Aggiungo gli utenti e i gruppi necessari per il corretto funzionamento di qmail"
    
    groupadd nofiles
    useradd -g nofiles -d /var/qmail/alias alias
    useradd -g nofiles -d /var/qmail qmaild
    useradd -g nofiles -d /var/qmail qmaill
    useradd -g nofiles -d /var/qmail qmailp
    groupadd qmail
    useradd -g qmail -d /var/qmail qmailq
    useradd -g qmail -d /var/qmail qmailr
    useradd -g qmail -d /var/qmail qmails
    
}

function build_qmail(){
    cd $qmail_installation_path$netqmail

    print_message 1 "Inizio compilazione qmail..."
    
    make setup check 

    if [ $? -ne 0 ]; then
	print_message 3 "Si è verificato un errore durante la compilazione di qmail" true
    else
	print_message 1 "qmail compilato correttamente"
    fi
    
}

function config_fast_qmail(){
    cd $qmail_installation_path$netqmail

    ./config-fast $hostname

    if [ $? -ne 0 ]; then
	print_message 3 "Si è verificato un errore durante la configurazione di qmail" true
    else
	print_message 1 "qmail configurato correttamente"
    fi    
} 

function build_ucspi-tcp(){
    cd $qmail_installation_path$ucspi

    print_message 1 "Applico patch a ucspi-tcp..."

    patch < $qmail_installation_path$netqmail"/other-patches/ucspi-tcp-0.88.errno.patch"

    if [ $? -ne 0 ]; then
	print_message 3 "Si è verificato un errore durante l'applicazione della patch" true
    else
	print_message 1 "Inizio compilazione ucspi-tcp..."
	make
	make setup check
	if [ $? -ne 0 ]; then
	    print_message 3 "Si è verificato un errore durante la compilazione di ucspi-tcp" true
	else
	    print_message 1 "ucspi-tcp compilato correttamente"
	fi    
	
    fi 
}

function build_daemontools(){
    cd $daemontools_package_installation_dir"admin/"$daemontools"/src"

    print_message 1 "Applico patch a daemontools..."

    patch < $qmail_installation_path$netqmail"/other-patches/daemontools-0.76.errno.patch"
    print_message 1 "Inizio compilazione daemontools..."

    cd $daemontools_package_installation_dir"admin/"$daemontools"/"

    package/install

    if [ $? -ne 0 ]; then
	print_message 3 "Si è verificato un errore durante la compilazione di daemontools" true
    else
	print_message 1 "daemontools compilato correttamente"
    fi 
}

# Cosa fa	        :		Applica un fix a daemontools per il corretto funzionamento su macchine CentOs-based.
#					Vedi: http://www.kluner.net/2011/04/04/daemontools-on-redhat-enterprise-6-0/
function fix_daemontools(){
    sed -i -r 's/^SV:[1-9]+:respawn:\/command\/svscanboot//g' /etc/inittab
    
    printf "start on runlevel [345]\nrespawn\nexec /command/svscanboot" > /etc/init/svscan.conf
    initctl reload-configuration
    initctl start svscan
}

# Cosa fa		:		Crea lo script per il boot di qmail
#					Vedi: http://www.lifewithqmail.org/lwq.html#start-qmail
function create_qmail_boot_script(){
    print_message 1 "Inizio creazione file di boot per qmail in /var/qmail/rc"

    printf "#!/bin/sh\nexec env - PATH=\"/var/qmail/bin:\$PATH\" qmail-start \"\`cat /var/qmail/control/defaultdelivery\`\"" > /var/qmail/rc
    
    chmod 755 /var/qmail/rc

    if [ $? -ne 0 ]; then
	print_message 3 "Si è verificato un errore durante il tentativo di cambiare i permessi al file /var/qmail/rc" true
    else
	print_message 1 "File di boot di qmail creato correttamente"
    fi 
    
    make_dir /var/log/qmail
}

function set_default_mailbox_delivery_format(){
    print_message 1 "Assegno default delivery"
    
    echo ./Maildir/ > /var/qmail/control/defaultdelivery
}

# Cosa fa		:		Scarica/genera i file di start-up per l'avvio di qmail e svscan al boot del sistema
function get_configure_startup_scripts(){
    print_message 1 "Copia startup script in corso..."
    
    cp $qmail_source_code_path$qmail_startup_script /var/qmail/bin/qmailctl

    if [ $? -ne 0 ]; then
	print_message 3 "Si è verificato un errore durante la copia dello script di startup per qmail" true
    else
	print_message 1 "Copia completata, cambio permessi del file di startup e creo un link simbolico in /usr/bin"
    fi 
    
    chmod 755 /var/qmail/bin/qmailctl
    ln -s /var/qmail/bin/qmailctl /usr/bin
}

# Cosa fa		:		Crea gli script per l'avvio del supervise script
function make_supervise_scripts(){
    print_message 1 "Creo le cartelle e gli script per il supervise..."
    
    make_dir /var/qmail/supervise/qmail-send/log
    make_dir /var/qmail/supervise/qmail-smtpd/log

    printf "#!/bin/sh\nexec /var/qmail/rc" > /var/qmail/supervise/qmail-send/run

    printf "#!/bin/sh\nexec /usr/local/bin/setuidgid qmaill /usr/local/bin/multilog t /var/log/qmail" > /var/qmail/supervise/qmail-send/log/run

    printf "#!/bin/sh\n
QMAILDUID=\`id -u qmaild\`\n
NOFILESGID=\`id -g qmaild\`\n
MAXSMTPD=\`cat /var/qmail/control/concurrencyincoming\`\n
LOCAL=\`head -1 /var/qmail/control/me\`\n

if [ -z \"\$QMAILDUID\" -o -z \"\$NOFILESGID\" -o -z \"\$MAXSMTPD\" -o -z \"\$LOCAL\" ]; then\n
    echo QMAILDUID, NOFILESGID, MAXSMTPD, or LOCAL is unset in\n
    echo /var/qmail/supervise/qmail-smtpd/run\n
    exit 1\n
fi\n

if [ ! -f /var/qmail/control/rcpthosts ]; then\n
    echo \"No /var/qmail/control/rcpthosts!\"\n
    echo \"Refusing to start SMTP listener because it'll create an open relay\"\n
    exit 1\n
fi\n

exec /usr/local/bin/softlimit -m 2000000 /usr/local/bin/tcpserver -v -R -l \"\$LOCAL\" -x /etc/tcp.smtp.cdb -c \"\$MAXSMTPD\" -u \"\$QMAILDUID\" -g \"\$NOFILESGID\" 0 smtp /var/qmail/bin/qmail-smtpd 2>&1" > /var/qmail/supervise/qmail-smtpd/run
}

function set_concurrencyincoming(){
    echo 20 > /var/qmail/control/concurrencyincoming
    chmod 644 /var/qmail/control/concurrencyincoming
}

function make_smtpd_scripts(){
    printf "#!/bin/sh\nexec /usr/local/bin/setuidgid qmaill /usr/local/bin/multilog t /var/log/qmail/smtpd" > /var/qmail/supervise/qmail-smtpd/log/run
    
}

function set_executable_scripts(){
    print_message 1 "Rendo eseguibili i file creati"
    chmod 755 /var/qmail/supervise/qmail-send/run
    chmod 755 /var/qmail/supervise/qmail-send/log/run
    chmod 755 /var/qmail/supervise/qmail-smtpd/run
    chmod 755 /var/qmail/supervise/qmail-smtpd/log/run
}

function make_aliases(){
    echo root > /var/qmail/alias/.qmail-postmaster
    chmod 644 /var/qmail/alias/.qmail-postmaster
}

# Cosa fa		:		Installa qmail utilizzando le configurazioni impostate
function install(){
    check_root_user

    print_message 1 "Controllo dipendenze..."
    
    for dependency in "${dependencies[@]}"; do
	check_dependency $dependency
    done 

    print_message 1 "Tutte le dipendenze risultano installate, procedo con l'installazione di qmail e dei pacchetti necessari"
    
    print_message 1 "Inizio installazione qmail"
    
    make_dir $qmail_source_code_path

    print_message 1 "Cambio directory in "$qmail_source_code_path
    cd $qmail_source_code_path
    
    for url in "${sources_file_urls[@]}"; do
	download_sources $url
    done 

    #    qmail_installation_path
    umask 022

    make_dir $qmail_installation_path

    make_dir $daemontools_package_installation_dir

    chmod 1755 /package

    mkdir /var/qmail
    
    explode_sources

    linux_add_users_group

    # Inizio la compilazione di qmail
    build_qmail

    config_fast_qmail

    # Compilazione ucspi-tcp
    build_ucspi-tcp

    # Compilazione daemontools
    build_daemontools

    fix_daemontools
    
    # Applico il fix a daemontools

    create_qmail_boot_script
    
    set_default_mailbox_delivery_format
    
    get_configure_startup_scripts

    make_supervise_scripts

    set_concurrencyincoming

    make_smtpd_scripts
    
    set_executable_scripts

    make_dir /var/log/qmail/smtpd
    chown qmaill /var/log/qmail /var/log/qmail/smtpd

    ln -s /var/qmail/supervise/qmail-send /var/qmail/supervise/qmail-smtpd /service
    initctl stop svscan
    initctl start svscan

    qmailctl stop

    echo '127.:allow,RELAYCLIENT=""' >>/etc/tcp.smtp
    qmailctl cdb

    echo root > /var/qmail/alias/.qmail-postmaster

    qmailctl start
    
    if [ $? -ne 0 ]; then
	print_message 3 "Si è verificato un errore durante l'avvio di qmail" true
    else
	print_message 1 "qmail installato, configurato e avviato correttamente"
    fi 

}

install
exit $? 

